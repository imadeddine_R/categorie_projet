package org.sid;

import org.sid.dao.ProduitRepository;
import org.sid.entities.Produit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class CataMvc4Application {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(CataMvc4Application.class, args);
		ProduitRepository produitRepository = ctx.getBean(ProduitRepository.class);
		produitRepository.save(new Produit("LX 4353", 678, 90));
		produitRepository.save(new Produit("Ord HP 5343", 780, 190));
		produitRepository.save(new Produit("Imprimante Epson ", 888, 110));
		produitRepository.save(new Produit("Imprimante HP ", 500, 130));
		produitRepository.findAll().forEach(p->System.out.println(p.getDesignation()));
		
		
				
	}
}
